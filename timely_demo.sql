/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : 127.0.0.1:3306
 Source Schema         : timely_demo

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 16/12/2019 23:56:10
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ty_admin
-- ----------------------------
DROP TABLE IF EXISTS `ty_admin`;
CREATE TABLE `ty_admin`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `account` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号',
  `password` varchar(155) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登录密码',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ty_admin
-- ----------------------------
INSERT INTO `ty_admin` VALUES (1, 'admin', 'E10ADC3949BA59ABBE56E057F20F883E', '2019-10-08 17:50:30');

-- ----------------------------
-- Table structure for ty_chat_log
-- ----------------------------
DROP TABLE IF EXISTS `ty_chat_log`;
CREATE TABLE `ty_chat_log`  (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志id',
  `from_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发送者id',
  `from_name` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发送者名称',
  `from_avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发送者头像',
  `to_id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接收方id',
  `to_name` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接受者名称',
  `to_avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '接收者头像',
  `message` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发送的内容',
  `send_status` tinyint(1) NULL DEFAULT 1 COMMENT '发送状态 1发送成功  2发送失败   可以重发',
  `read_flag` tinyint(1) NULL DEFAULT 1 COMMENT '是否已读 1 未读 2 已读',
  `create_time` datetime(0) NOT NULL COMMENT '记录时间',
  PRIMARY KEY (`log_id`) USING BTREE,
  INDEX `from_id`(`from_id`) USING BTREE,
  INDEX `to_id`(`to_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 220 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ty_chat_log
-- ----------------------------
INSERT INTO `ty_chat_log` VALUES (120, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '尔特人', 1, 1, '2019-10-30 18:28:48');
INSERT INTO `ty_chat_log` VALUES (121, '225db946744b2ca', '游客225db946744b2ca', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '尔特人', 1, 1, '2019-10-30 18:28:53');
INSERT INTO `ty_chat_log` VALUES (122, '225db946744b2ca', '游客225db946744b2ca', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '1212', 1, 1, '2019-10-30 18:29:23');
INSERT INTO `ty_chat_log` VALUES (123, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '是的发生的', 1, 1, '2019-10-30 18:29:30');
INSERT INTO `ty_chat_log` VALUES (124, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '1212', 1, 1, '2019-10-31 10:16:34');
INSERT INTO `ty_chat_log` VALUES (125, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '1212', 1, 1, '2019-10-31 10:25:07');
INSERT INTO `ty_chat_log` VALUES (126, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '12121', 1, 1, '2019-10-31 10:25:11');
INSERT INTO `ty_chat_log` VALUES (127, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '34343', 2, 1, '2019-10-31 10:25:21');
INSERT INTO `ty_chat_log` VALUES (128, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '3434', 2, 1, '2019-10-31 10:25:22');
INSERT INTO `ty_chat_log` VALUES (129, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '3434', 2, 1, '2019-10-31 10:25:23');
INSERT INTO `ty_chat_log` VALUES (130, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '3434', 2, 1, '2019-10-31 10:25:24');
INSERT INTO `ty_chat_log` VALUES (131, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '3434', 2, 1, '2019-10-31 10:25:25');
INSERT INTO `ty_chat_log` VALUES (132, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '2323', 1, 1, '2019-10-31 10:29:45');
INSERT INTO `ty_chat_log` VALUES (133, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '2323', 1, 1, '2019-10-31 10:36:50');
INSERT INTO `ty_chat_log` VALUES (134, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '234儿童隔热个梵蒂冈', 1, 1, '2019-10-31 10:36:54');
INSERT INTO `ty_chat_log` VALUES (135, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '舒服舒服', 1, 1, '2019-10-31 10:43:49');
INSERT INTO `ty_chat_log` VALUES (136, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '3434', 1, 1, '2019-10-31 10:44:12');
INSERT INTO `ty_chat_log` VALUES (137, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '1212', 1, 1, '2019-10-31 10:53:10');
INSERT INTO `ty_chat_log` VALUES (138, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '热污染', 1, 1, '2019-10-31 10:54:35');
INSERT INTO `ty_chat_log` VALUES (139, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', 'rete', 1, 1, '2019-10-31 10:54:44');
INSERT INTO `ty_chat_log` VALUES (140, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '2323', 1, 1, '2019-10-31 11:01:07');
INSERT INTO `ty_chat_log` VALUES (141, '225db946744b2ca', '游客225db946744b2ca', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '1212', 1, 1, '2019-10-31 11:01:23');
INSERT INTO `ty_chat_log` VALUES (142, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', 'dgdg', 1, 1, '2019-10-31 12:42:33');
INSERT INTO `ty_chat_log` VALUES (143, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '212', 1, 1, '2019-10-31 12:53:27');
INSERT INTO `ty_chat_log` VALUES (144, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '4545', 1, 1, '2019-10-31 12:53:31');
INSERT INTO `ty_chat_log` VALUES (145, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', 'dfdf', 1, 1, '2019-10-31 12:53:39');
INSERT INTO `ty_chat_log` VALUES (146, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', 'sfsf', 1, 1, '2019-10-31 12:53:46');
INSERT INTO `ty_chat_log` VALUES (147, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '2323', 1, 1, '2019-10-31 12:54:39');
INSERT INTO `ty_chat_log` VALUES (148, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '3434', 1, 1, '2019-10-31 12:55:40');
INSERT INTO `ty_chat_log` VALUES (149, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '3434', 1, 1, '2019-10-31 12:55:42');
INSERT INTO `ty_chat_log` VALUES (150, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '2323', 1, 1, '2019-10-31 12:56:20');
INSERT INTO `ty_chat_log` VALUES (151, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '2323', 2, 1, '2019-10-31 12:58:21');
INSERT INTO `ty_chat_log` VALUES (152, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '2332', 1, 1, '2019-10-31 12:59:14');
INSERT INTO `ty_chat_log` VALUES (153, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '2323', 1, 1, '2019-10-31 12:59:21');
INSERT INTO `ty_chat_log` VALUES (154, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '2323', 2, 1, '2019-10-31 13:01:41');
INSERT INTO `ty_chat_log` VALUES (155, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '2323', 2, 1, '2019-10-31 13:01:50');
INSERT INTO `ty_chat_log` VALUES (156, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '2323', 1, 1, '2019-10-31 13:01:54');
INSERT INTO `ty_chat_log` VALUES (157, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'dfgd', 1, 1, '2019-10-31 13:02:13');
INSERT INTO `ty_chat_log` VALUES (158, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'sdfsdf', 2, 1, '2019-10-31 13:02:16');
INSERT INTO `ty_chat_log` VALUES (159, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '2323', 2, 1, '2019-10-31 13:04:53');
INSERT INTO `ty_chat_log` VALUES (160, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '2323', 1, 1, '2019-10-31 13:05:04');
INSERT INTO `ty_chat_log` VALUES (161, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '2323', 1, 1, '2019-10-31 13:05:07');
INSERT INTO `ty_chat_log` VALUES (162, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '2323', 1, 1, '2019-10-31 13:05:16');
INSERT INTO `ty_chat_log` VALUES (163, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'sdfsd', 1, 1, '2019-10-31 13:05:40');
INSERT INTO `ty_chat_log` VALUES (164, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'dfgf', 1, 1, '2019-10-31 13:05:49');
INSERT INTO `ty_chat_log` VALUES (165, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'er', 1, 1, '2019-10-31 13:06:41');
INSERT INTO `ty_chat_log` VALUES (166, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '2323', 2, 1, '2019-10-31 13:08:02');
INSERT INTO `ty_chat_log` VALUES (167, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'dgd', 2, 1, '2019-10-31 13:08:12');
INSERT INTO `ty_chat_log` VALUES (168, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'dgd', 2, 1, '2019-10-31 13:08:15');
INSERT INTO `ty_chat_log` VALUES (169, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '2323', 1, 1, '2019-10-31 13:08:50');
INSERT INTO `ty_chat_log` VALUES (170, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '232', 1, 1, '2019-10-31 13:08:54');
INSERT INTO `ty_chat_log` VALUES (171, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '232323', 1, 1, '2019-10-31 13:08:58');
INSERT INTO `ty_chat_log` VALUES (172, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '23233', 1, 1, '2019-10-31 13:09:27');
INSERT INTO `ty_chat_log` VALUES (173, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'gfdg', 1, 1, '2019-10-31 13:10:52');
INSERT INTO `ty_chat_log` VALUES (174, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '3434', 1, 1, '2019-10-31 13:12:07');
INSERT INTO `ty_chat_log` VALUES (175, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'ter', 1, 1, '2019-10-31 13:12:15');
INSERT INTO `ty_chat_log` VALUES (176, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '4545', 1, 1, '2019-10-31 13:12:24');
INSERT INTO `ty_chat_log` VALUES (177, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '343', 1, 1, '2019-10-31 13:13:48');
INSERT INTO `ty_chat_log` VALUES (178, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '343', 1, 1, '2019-10-31 13:17:02');
INSERT INTO `ty_chat_log` VALUES (179, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'tryrty', 1, 1, '2019-10-31 13:19:13');
INSERT INTO `ty_chat_log` VALUES (180, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '3434', 1, 1, '2019-10-31 13:24:01');
INSERT INTO `ty_chat_log` VALUES (181, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '43434', 1, 1, '2019-10-31 13:24:05');
INSERT INTO `ty_chat_log` VALUES (182, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '434', 1, 1, '2019-10-31 13:24:10');
INSERT INTO `ty_chat_log` VALUES (183, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '343434', 1, 1, '2019-10-31 13:24:14');
INSERT INTO `ty_chat_log` VALUES (184, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '232323', 2, 1, '2019-10-31 13:24:36');
INSERT INTO `ty_chat_log` VALUES (185, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '454', 1, 1, '2019-10-31 13:24:42');
INSERT INTO `ty_chat_log` VALUES (186, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '4545', 1, 1, '2019-10-31 13:24:45');
INSERT INTO `ty_chat_log` VALUES (187, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '454', 1, 1, '2019-10-31 13:24:48');
INSERT INTO `ty_chat_log` VALUES (188, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '4545', 1, 1, '2019-10-31 13:24:51');
INSERT INTO `ty_chat_log` VALUES (189, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '4545', 1, 1, '2019-10-31 13:24:53');
INSERT INTO `ty_chat_log` VALUES (190, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '4545554', 1, 1, '2019-10-31 13:24:56');
INSERT INTO `ty_chat_log` VALUES (191, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '45454', 1, 1, '2019-10-31 13:24:58');
INSERT INTO `ty_chat_log` VALUES (192, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', 'rtyt', 1, 1, '2019-10-31 13:25:02');
INSERT INTO `ty_chat_log` VALUES (193, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'tyrty', 1, 1, '2019-10-31 13:25:10');
INSERT INTO `ty_chat_log` VALUES (194, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '3434', 1, 1, '2019-10-31 13:26:17');
INSERT INTO `ty_chat_log` VALUES (195, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'grty', 1, 1, '2019-10-31 13:26:21');
INSERT INTO `ty_chat_log` VALUES (196, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'gjgj', 1, 1, '2019-10-31 13:26:35');
INSERT INTO `ty_chat_log` VALUES (197, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', 'ghgj', 1, 1, '2019-10-31 13:26:39');
INSERT INTO `ty_chat_log` VALUES (198, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'fghfg', 2, 1, '2019-10-31 13:26:47');
INSERT INTO `ty_chat_log` VALUES (199, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', 'rtytr', 1, 1, '2019-10-31 13:26:52');
INSERT INTO `ty_chat_log` VALUES (200, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'rtytr', 1, 1, '2019-10-31 13:26:56');
INSERT INTO `ty_chat_log` VALUES (201, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '2323', 1, 1, '2019-10-31 13:38:43');
INSERT INTO `ty_chat_log` VALUES (202, 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '1212', 1, 1, '2019-10-31 13:38:46');
INSERT INTO `ty_chat_log` VALUES (203, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '2323', 1, 1, '2019-10-31 14:01:18');
INSERT INTO `ty_chat_log` VALUES (204, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5db8eafbe332f', '尔耳', '/static/common/images/kefu.jpg', '电饭锅电饭锅的', 1, 1, '2019-10-31 14:01:24');
INSERT INTO `ty_chat_log` VALUES (205, 'KF_kefu5dbbbf322d050', '而耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '2323', 1, 1, '2019-11-01 13:18:34');
INSERT INTO `ty_chat_log` VALUES (206, 'KF_kefu5dbbbf322d050', '而耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '2323', 1, 1, '2019-11-01 13:19:59');
INSERT INTO `ty_chat_log` VALUES (207, 'KF_kefu5dbbbf322d050', '而耳', '/static/common/images/kefu.jpg', '235dbbc01213539', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '2323', 1, 1, '2019-11-01 13:20:03');
INSERT INTO `ty_chat_log` VALUES (208, 'KF_kefu5dbbbf322d050', '而耳', '/static/common/images/kefu.jpg', '235dbbc01213539', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '2323', 1, 1, '2019-11-01 13:20:10');
INSERT INTO `ty_chat_log` VALUES (209, 'KF_kefu5dbbbf322d050', '而耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '434', 1, 1, '2019-11-01 13:21:27');
INSERT INTO `ty_chat_log` VALUES (210, 'KF_kefu5dbbbf322d050', '而耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '2323', 1, 1, '2019-11-01 13:21:35');
INSERT INTO `ty_chat_log` VALUES (211, 'KF_kefu5dbbbf322d050', '而耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '2323', 1, 1, '2019-11-01 13:21:38');
INSERT INTO `ty_chat_log` VALUES (212, 'KF_kefu5dbbbf322d050', '而耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '2323', 1, 1, '2019-11-01 13:21:39');
INSERT INTO `ty_chat_log` VALUES (213, 'KF_kefu5dbbbf322d050', '而耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '2323', 1, 1, '2019-11-01 13:21:40');
INSERT INTO `ty_chat_log` VALUES (214, 'KF_kefu5dbbbf322d050', '而耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '2323', 1, 1, '2019-11-01 13:21:42');
INSERT INTO `ty_chat_log` VALUES (215, 'KF_kefu5dbbbf322d050', '而耳', '/static/common/images/kefu.jpg', '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '2323', 1, 1, '2019-11-01 13:44:44');
INSERT INTO `ty_chat_log` VALUES (216, '235dbbc01213539', '游客235dbbc01213539', '/static/common/images/visitor.jpg', 'KF_kefu5dbbbf322d050', '而耳', '/static/common/images/kefu.jpg', '2122', 1, 1, '2019-11-01 14:22:50');
INSERT INTO `ty_chat_log` VALUES (217, '235dbbc01213539', '游客235dbbc01213539', '/static/common/images/visitor.jpg', 'KF_kefu5dbf961558d32', '22', '/static/common/images/kefu.jpg', '1212', 1, 1, '2019-11-04 11:09:03');
INSERT INTO `ty_chat_log` VALUES (218, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', 'KF_kefu5dbf961558d32', '22', '/static/common/images/kefu.jpg', '4545', 1, 1, '2019-11-04 11:09:14');
INSERT INTO `ty_chat_log` VALUES (219, '235dbbc01213539', '游客235dbbc01213539', '/static/common/images/visitor.jpg', 'KF_kefu5dbf961558d32', '22', '/static/common/images/kefu.jpg', '2323', 1, 1, '2019-11-04 11:09:24');

-- ----------------------------
-- Table structure for ty_kefu_info
-- ----------------------------
DROP TABLE IF EXISTS `ty_kefu_info`;
CREATE TABLE `ty_kefu_info`  (
  `kefu_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '客服id',
  `kefu_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客服唯一标识',
  `kefu_name` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客服名称',
  `kefu_avatar` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客服头像',
  `kefu_password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客服密码',
  `online_status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '在线状态 1 在线 2 离线',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '更新时间',
  `client_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客服登录标示',
  PRIMARY KEY (`kefu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ty_kefu_info
-- ----------------------------
INSERT INTO `ty_kefu_info` VALUES (30, 'kefu5df785ee07ae1', '客服一', '/static/common/images/kefu.jpg', 'E10ADC3949BA59ABBE56E057F20F883E', 2, '2019-12-16 21:26:06', '2019-12-16 21:26:06', NULL);
INSERT INTO `ty_kefu_info` VALUES (31, 'kefu5df786c6a2277', '客服三', '/static/common/images/kefu.jpg', 'D41D8CD98F00B204E9800998ECF8427E', 2, '2019-12-16 21:29:42', '2019-12-16 21:50:27', NULL);

-- ----------------------------
-- Table structure for ty_visitor
-- ----------------------------
DROP TABLE IF EXISTS `ty_visitor`;
CREATE TABLE `ty_visitor`  (
  `vid` int(11) NOT NULL AUTO_INCREMENT,
  `visitor_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客id',
  `visitor_name` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客名称',
  `visitor_avatar` varchar(155) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客头像',
  `visitor_ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '访客ip',
  `client_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户端标识',
  `online_status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0 离线 1 在线',
  `create_time` datetime(0) NOT NULL COMMENT '访问时间',
  PRIMARY KEY (`vid`) USING BTREE,
  INDEX `visiter`(`visitor_id`) USING BTREE,
  INDEX `time`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 196 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ty_visitor
-- ----------------------------
INSERT INTO `ty_visitor` VALUES (192, '225db94641ddafe', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '127.0.0.1', '0', 2, '2019-11-04 11:09:12');
INSERT INTO `ty_visitor` VALUES (193, '225db946744b2ca', '游客225db946744b2ca', '/static/common/images/visitor.jpg', '127.0.0.1', '0', 2, '2019-10-31 11:57:29');
INSERT INTO `ty_visitor` VALUES (195, '235dbbc01213539', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', '0', 2, '2019-11-04 13:48:11');

-- ----------------------------
-- Table structure for ty_visitor_queue
-- ----------------------------
DROP TABLE IF EXISTS `ty_visitor_queue`;
CREATE TABLE `ty_visitor_queue`  (
  `qid` int(11) NOT NULL AUTO_INCREMENT COMMENT '队列id',
  `visitor_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客id',
  `visitor_name` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客名称',
  `visitor_avatar` varchar(155) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客头像',
  `visitor_ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '访客ip',
  `client_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '客户端标识',
  `create_time` datetime(0) NOT NULL COMMENT '访问时间',
  `reception_status` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '接待状态  0 等待接待中 1 接待中  2接待完成',
  `kefu_code` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kefu_client_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`qid`) USING BTREE,
  UNIQUE INDEX `id`(`visitor_id`) USING BTREE,
  INDEX `visiter`(`visitor_id`) USING BTREE,
  INDEX `time`(`create_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ty_visitor_queue
-- ----------------------------
INSERT INTO `ty_visitor_queue` VALUES (9, '225db946744b2ca', '游客225db946744b2ca', '/static/common/images/visitor.jpg', '127.0.0.1', '0', '2019-10-31 11:57:29', 2, 'kefu5db8eafbe332f', '40');
INSERT INTO `ty_visitor_queue` VALUES (12, '235dbbc01213539', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', '0', '2019-11-04 13:48:11', 2, 'kefu5dbf961558d32', '16');

-- ----------------------------
-- Table structure for ty_visitor_service_log
-- ----------------------------
DROP TABLE IF EXISTS `ty_visitor_service_log`;
CREATE TABLE `ty_visitor_service_log`  (
  `vsid` int(11) NOT NULL AUTO_INCREMENT COMMENT '服务编号',
  `visitor_id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客id',
  `client_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客的客户端标识',
  `visitor_name` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客名称',
  `visitor_avatar` varchar(155) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客头像',
  `visitor_ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '访客的ip',
  `kefu_id` int(11) NOT NULL,
  `kefu_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '接待的客服标识',
  `kefu_name` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客服名称',
  `start_date` datetime(0) NOT NULL COMMENT '开始服务时间',
  `end_date` datetime(0) NULL DEFAULT NULL COMMENT '结束服务时间',
  `connect_stauts` tinyint(3) NOT NULL DEFAULT 1 COMMENT '连接状态  1 正在连接  2 关闭连接',
  PRIMARY KEY (`vsid`) USING BTREE,
  INDEX `user_id,client_id`(`visitor_id`, `client_id`) USING BTREE,
  INDEX `kf_id,start_time,end_time`(`kefu_code`, `start_date`, `end_date`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3270 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ty_visitor_service_log
-- ----------------------------
INSERT INTO `ty_visitor_service_log` VALUES (3236, '235dbbc01213539', '29', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 13:21:29', '2019-11-01 13:21:47', 2);
INSERT INTO `ty_visitor_service_log` VALUES (3237, '235dbbc01213539', '30', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 13:21:47', '2019-11-01 13:31:56', 2);
INSERT INTO `ty_visitor_service_log` VALUES (3238, '225db94641ddafe', '32', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 13:31:48', NULL, 1);
INSERT INTO `ty_visitor_service_log` VALUES (3239, '235dbbc01213539', '33', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 13:31:57', '2019-11-01 13:31:57', 2);
INSERT INTO `ty_visitor_service_log` VALUES (3240, '235dbbc01213539', '34', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 13:31:58', '2019-11-01 13:44:33', 2);
INSERT INTO `ty_visitor_service_log` VALUES (3241, '235dbbc01213539', '36', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 13:44:34', '2019-11-01 13:44:46', 2);
INSERT INTO `ty_visitor_service_log` VALUES (3242, '235dbbc01213539', '38', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 13:44:46', NULL, 1);
INSERT INTO `ty_visitor_service_log` VALUES (3243, '225db94641ddafe', '60', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:13:22', NULL, 1);
INSERT INTO `ty_visitor_service_log` VALUES (3244, '235dbbc01213539', '67', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:14:02', NULL, 1);
INSERT INTO `ty_visitor_service_log` VALUES (3245, '225db94641ddafe', '69', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:14:38', '2019-11-01 14:14:45', 2);
INSERT INTO `ty_visitor_service_log` VALUES (3246, '225db94641ddafe', '71', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:14:46', '2019-11-01 14:14:50', 2);
INSERT INTO `ty_visitor_service_log` VALUES (3247, '235dbbc01213539', '2', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:22:44', '2019-11-01 14:22:47', 2);
INSERT INTO `ty_visitor_service_log` VALUES (3248, '235dbbc01213539', '3', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:22:47', '2019-11-01 14:22:53', 2);
INSERT INTO `ty_visitor_service_log` VALUES (3249, '235dbbc01213539', '4', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:22:59', '2019-11-01 14:23:03', 2);
INSERT INTO `ty_visitor_service_log` VALUES (3250, '235dbbc01213539', '5', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:22:59', NULL, 1);
INSERT INTO `ty_visitor_service_log` VALUES (3251, '235dbbc01213539', '6', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:23:03', '2019-11-01 14:23:03', 2);
INSERT INTO `ty_visitor_service_log` VALUES (3252, '235dbbc01213539', '7', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:23:03', NULL, 1);
INSERT INTO `ty_visitor_service_log` VALUES (3253, '235dbbc01213539', '8', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:23:03', NULL, 1);
INSERT INTO `ty_visitor_service_log` VALUES (3254, '235dbbc01213539', '9', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:23:03', NULL, 1);
INSERT INTO `ty_visitor_service_log` VALUES (3255, '235dbbc01213539', '10', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:23:03', NULL, 1);
INSERT INTO `ty_visitor_service_log` VALUES (3256, '235dbbc01213539', '11', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:23:03', NULL, 1);
INSERT INTO `ty_visitor_service_log` VALUES (3257, '235dbbc01213539', '12', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:23:03', NULL, 1);
INSERT INTO `ty_visitor_service_log` VALUES (3258, '235dbbc01213539', '13', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:23:04', '2019-11-01 14:23:16', 2);
INSERT INTO `ty_visitor_service_log` VALUES (3259, '235dbbc01213539', '14', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:23:04', NULL, 1);
INSERT INTO `ty_visitor_service_log` VALUES (3260, '235dbbc01213539', '15', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:23:04', NULL, 1);
INSERT INTO `ty_visitor_service_log` VALUES (3261, '235dbbc01213539', '16', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:23:22', '2019-11-01 14:32:19', 2);
INSERT INTO `ty_visitor_service_log` VALUES (3262, '235dbbc01213539', '17', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:32:25', '2019-11-01 14:32:39', 2);
INSERT INTO `ty_visitor_service_log` VALUES (3263, '235dbbc01213539', '19', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:32:45', '2019-11-01 15:11:53', 2);
INSERT INTO `ty_visitor_service_log` VALUES (3264, '225db94641ddafe', '20', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '127.0.0.1', 23, 'kefu5dbbbf322d050', NULL, '2019-11-01 14:32:50', '2019-11-01 15:11:50', 2);
INSERT INTO `ty_visitor_service_log` VALUES (3265, '235dbbc01213539', '2', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 24, 'kefu5dbf961558d32', NULL, '2019-11-04 11:09:01', '2019-11-04 11:09:22', 2);
INSERT INTO `ty_visitor_service_log` VALUES (3266, '225db94641ddafe', '3', '游客225db94641ddafe', '/static/common/images/visitor.jpg', '127.0.0.1', 24, 'kefu5dbf961558d32', NULL, '2019-11-04 11:09:12', '2019-11-04 11:09:26', 2);
INSERT INTO `ty_visitor_service_log` VALUES (3267, '235dbbc01213539', '4', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 24, 'kefu5dbf961558d32', NULL, '2019-11-04 11:09:22', NULL, 1);
INSERT INTO `ty_visitor_service_log` VALUES (3268, '235dbbc01213539', '2', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 24, 'kefu5dbf961558d32', NULL, '2019-11-04 13:46:46', '2019-11-04 13:47:37', 2);
INSERT INTO `ty_visitor_service_log` VALUES (3269, '235dbbc01213539', '7', '游客235dbbc01213539', '/static/common/images/visitor.jpg', '127.0.0.1', 24, 'kefu5dbf961558d32', NULL, '2019-11-04 13:48:11', '2019-11-04 13:50:53', 2);

SET FOREIGN_KEY_CHECKS = 1;
